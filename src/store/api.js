import api from '../config/api.js';

const store = new Proxy(api, {
    get(target, prop, receiver) {
        if (prop in target) {
            return Reflect.get(target, prop, receiver);
        }
        console.log(`${prop} 未定义`);
    },

    set(target, prop, value, receiver) {
        if (prop in target) {
            console.log(`${target[prop]} -> ${value}`);
            return Reflect.set(target, prop, value, receiver)
        }
        console.log(`${prop} 未定义`);
    }
});

export default store;