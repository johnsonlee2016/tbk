import manage from '../utils/user.js';


class User {

    static get data(){
        return  manage.read();
    }

    static get uid() {
        return User.data['id'];
    }

    static get nick() {
        return User.data['nick'];
    }

    static get account() {
        return User.data['account']
    }
}

export default User;