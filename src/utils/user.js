class User {
    static handle = 'USER-TOKEN';

    static write(data) {
        data = JSON.stringify(data);
        return sessionStorage.setItem(User.handle,data) || localStorage.setItem(User.handle,data);
    }


    static read() {
        let data = sessionStorage.getItem(User.handle) || localStorage.getItem(User.handle);
        return data && JSON.parse(data);
    }


    static clear() {
        sessionStorage.clear();
        localStorage.clear();
    }
}

export default User;