import Vue from 'vue'
import Router from 'vue-router'
import config from './config/router.js'
import User from "./utils/user";

Vue.use(Router);

const router = new Router(config);

router.beforeEach((to, from, next) => {
    let validator = typeof to.meta.auth == "undefined"
        || !to.meta.auth
        || User.read();
    let result = validator ? {} : {
        name: "login",
        query: {
            url: to.fullPath
        }
    };
    next(result);
});

export default router;