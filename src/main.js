import Vue from 'vue';
import App from './App.vue';
import router from './router';
import http from './config/axios.js';
import qs from 'qs';
import {Lazyload} from 'vant';
import api from './store/api.js';
import user from './store/user.js'

Vue.config.productionTip = false;
Vue.prototype.$http = http;
Vue.prototype.$qs = qs;
Vue.prototype.$store = {
    api,
    user
};

Vue.use(Lazyload);

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
