import Axios from 'axios';
import Qs from 'qs';
import Store from '../utils/store.js'

let http = Axios.create({

    baseURL: process.env.VUE_APP_BASE_URL,
    // 超时设置
    timeout: 3000,

    // 数据响应格式
    responseType: 'json',

    // 数据编码格式
    responseEncoding: 'utf8',

    // 只能用在 'PUT', 'POST' 和 'PATCH' 这几个请求方法
    transformRequest: [function (data, headers) {
        // application/x-www-form-urlencoded 编码格式
        return Qs.stringify(data);
    }],

    // `transformResponse` 在传递给 then/catch 前，允许修改响应数据
    transformResponse: [function (result) {
        // 对 static 进行任意转换处理
        return result;
    }],

    // get请求时 参数编码
    paramsSerializer: function (params) {
        // application/x-www-form-urlencoded 编码格式
        return Qs.stringify(params, {arrayFormat: 'brackets'})
    }
});

/**
 * 本地缓存
 * @param url
 * @param params
 * @return {Promise<any>}
 */
http.getStore = async function (url, params) {
    let full = url + Qs.stringify(params, {addQueryPrefix: true});
    let result = Store.get(full);
    if (!result) {
        result = await http.get(url, {params});
        Store.set(full, result);
    }
    return result
};

export default http;