const api = {
    'list': process.env.VUE_APP_API_LIST,// 商品类目
    'search': process.env.VUE_APP_API_SEARCH,  // 搜索
    'token':process.env.VUE_APP_API_TOKEN, //  淘口令

    'slide': process.env.VUE_APP_API_SLIDE,// 幻灯
};

export default api;