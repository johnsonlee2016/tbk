import layout from "@/views/layout.vue";
import index from "@/views/index.vue";

export default {
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            component: layout,
            children: [
                {
                    path: "/",
                    name: "index",
                    component: index,
                    meta: {
                        title: "优惠券"
                    }
                },
                {
                    path: "list",
                    name: "list",
                    meta: {
                        title: "好券直播"
                    },
                    component: () => import("@/views/list.vue")
                },
                {
                    path: "da_e_list",
                    name: "da_e_list",
                    meta: {
                        title: "大额优惠券"
                    },
                    component: () => import("@/views/da_e_list.vue")
                },
                {
                    path: "pin_pai_list",
                    name: "pin_pai_list",
                    meta: {
                        title: "品牌优惠券"
                    },
                    component: () => import("@/views/pin_pai_list.vue")
                },
                {
                    path: "liu_xing_list",
                    name: "liu_xing_list",
                    meta: {
                        title: "潮流趋势"
                    },
                    component: () => import("@/views/liu_xing_list.vue")
                },
                {
                    path: "te_hui_list",
                    name: "te_hui_list",
                    meta: {
                        title: "特惠商品优惠券"
                    },
                    component: () => import("@/views/te_hui_list.vue")
                },
                {
                    path: 'search',
                    name: 'search',
                    meta:{
                        title:"搜索页面"
                    },
                    component: () => import("@/views/search.vue")
                },
                {
                    path: "activity",
                    name: "activity",
                    meta: {
                        title: "聚划算拼团"
                    },
                    component: () => import('@/views/activity.vue')

                },
                {
                    path: 'page/:id',
                    name: 'page',
                    meta: {
                        title: "商品信息"
                    },
                    component: () => import('@/views/page.vue')
                }
            ],
        },
        {
            path: "*",
            component: () => import("@/views/404.vue")
        }
    ]
}