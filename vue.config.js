module.exports = {
    publicPath: process.env.VUE_APP_PUBLICE,
    devServer: {
        port: "3398",
        proxy: {
            '/api': {
                target: "http://127.0.0.1:3000",
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    }
};